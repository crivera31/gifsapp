import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Gif, SearchGifsResponse} from "../interfaces/gif.interface";

@Injectable({
  providedIn: 'root'
})
export class GifsService {

  private _apiKey: string = 'UWrmJx5lCaNvNGltL941bE5UsJ5BAAW5';
  private _urlBase: string = 'http://api.giphy.com/v1/gifs';
  private _historial: string[] = [];
  public resultados: Gif[] = [];

  constructor(private _http: HttpClient ) {
    /**1 forma */
    this._historial = JSON.parse(localStorage.getItem('historial')!) || [];
    this.resultados = JSON.parse(localStorage.getItem('resultados')!) || [];
    /**2 forma */
    // if(localStorage.getItem('historial')) {
    //   this._historial = JSON.parse(localStorage.getItem('historial')!);
    // }
  }

  get historial() {
    return [...this._historial];
  }

  buscarGifs(query: string = '') {
    query = query.trim().toLowerCase();
    if (!this._historial.includes(query)) {
      this._historial.unshift(query);
      this._historial = this._historial.splice(0, 10);
      localStorage.setItem('historial',JSON.stringify(this._historial));
    }
    console.log(this._historial);

    const params = new HttpParams()
            .set('api_key', this._apiKey)
            .set('limit', '10')
            .set('q', query);

    this._http.get<SearchGifsResponse>(`${this._urlBase}/search`,{ params }).subscribe(
      res => {
        console.log(res.data);
        this.resultados = res.data;
        localStorage.setItem('resultados',JSON.stringify(this.resultados));
      }
    )
  }
}
